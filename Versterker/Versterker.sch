EESchema Schematic File Version 4
LIBS:Versterker-cache
EELAYER 26 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 6
Title "Versterker"
Date "2018-11-15"
Rev "0.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 7800 5800 3700 2950
U 5BEEDEDF
F0 "eindversterker" 50
F1 "eindversterker.sch" 50
F2 "signaal" I L 7800 7200 50 
F3 "speakerout" O R 11500 7250 50 
F4 "speakerground" O R 11500 7500 50 
$EndSheet
$Sheet
S 750  750  1650 1500
U 5BEFC483
F0 "Voeding" 50
F1 "Voeding.sch" 50
$EndSheet
$Sheet
S 2550 750  800  500 
U 5BF5DEF8
F0 "Ingangstrap" 50
F1 "Ingangstrap.sch" 50
F2 "Signal1Out" O R 3350 900 50 
F3 "Signal2Out" O R 3350 1100 50 
$EndSheet
$Sheet
S 5500 2600 1200 1500
U 5BF29BAC
F0 "VUMeterJesse" 50
F1 "VUMeterJesse.sch" 50
F2 "SignaIn" I L 5500 3300 50 
$EndSheet
$Sheet
S 3900 750  1200 1500
U 5BF29BB3
F0 "MixerJesse" 50
F1 "MixerJesse.sch" 50
F2 "Signal1In" I L 3900 900 50 
F3 "Signal2In" I L 3900 1100 50 
F4 "SignalMixed" O R 5100 1450 50 
$EndSheet
Wire Wire Line
	3350 900  3900 900 
Wire Wire Line
	3350 1100 3900 1100
Wire Wire Line
	5100 1450 5300 1450
Wire Wire Line
	5300 1450 5300 3300
Wire Wire Line
	5300 3300 5500 3300
$EndSCHEMATC
