EESchema Schematic File Version 4
LIBS:Versterker-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:LM741 U?
U 1 1 5BF5DF39
P 1600 4250
F 0 "U?" H 1941 4296 50  0000 L CNN
F 1 "LM741" H 1941 4205 50  0000 L CNN
F 2 "" H 1650 4300 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm741.pdf" H 1750 4400 50  0001 C CNN
	1    1600 4250
	1    0    0    -1  
$EndComp
Text HLabel 3500 3550 2    50   Output ~ 0
Signal1Out
Text HLabel 3500 4000 2    50   Output ~ 0
Signal2Out
$EndSCHEMATC
