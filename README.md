# Versterker
Reference values
Weerstanden:    R1 - R7
Condensatoren:  C1 - C7
LEDS:           D1
Schakelaars:    SW1
Batterijen:     BT1 - BT2
Chips:          U1  - U2

//Nog toevoegen voor eindversterker

# Voeding

  Inputs: 
    Batterijvoeding+
    BatterijvoedingGND
    ExterneVoeding+
    ExterneVoedingGND
    
  Outputs:
    7V_VCC
    5V_VCC
    GND
  
  Reference values:
    Weerstanden:    R1 - R7
    Condensatoren:  C1 - C7
    LEDS:           D1
    Schakelaars:    SW1
    Batterijen:     BT1 - BT2
    Chips:          U1  - U2
    
# Vu-Meter

  Inputs: 

  Outputs:

  Reference values:
    Weerstanden:    
    Condensatoren:  
    LEDS:           
    Schakelaars:    
    Batterijen:     
    Chips:    
    
# Mixer

  Inputs: 

  Outputs:

  Reference values:
    Weerstanden:    
    Condensatoren:  
    LEDS:           
    Schakelaars:    
    Batterijen:     
    Chips:      

# Ingangstrap

  Inputs: 

  Outputs:

  Reference values:
    Weerstanden:    
    Condensatoren:  
    LEDS:           
    Schakelaars:    
    Batterijen:     
    Chips:  
    
# Equalizer

  Inputs: 

  Outputs:

  Reference values:
    Weerstanden:    
    Condensatoren:  
    LEDS:           
    Schakelaars:    
    Batterijen:     
    Chips:  
    
# Eindversterker
 
  Inputs:
    VCC
    GND
    Signaal
    
  Outputs:
    SpeakerSignaal
    SpeakerGND
    
  Reference values:
    Weerstanden:    
    Condensatoren:  
    LEDS:           
    Dioden:    
    Batterijen:     
    Chips:   
    
# Speaker

  Inputs: 
    SpeakerSignaal
    SpeakerGND
    
